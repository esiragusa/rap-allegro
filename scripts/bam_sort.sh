if [ "${EXPERIMENT_TYPE}" = "rabema" ]; then
    order="-n"
fi

CMD+="
${SCRIPTS_BIN_DIR}/samtools sort -@ ${threads} -m 3G ${order} ${PREFIX}.bam ${PREFIX}.sorted
dont_measure mv ${PREFIX}.sorted.bam ${PREFIX}.bam"
