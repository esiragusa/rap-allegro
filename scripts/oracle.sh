unset CMD

if [ ! -f ${RESULTS_DIR}/${experiment}.oracle.bam ] || [ ${ORACLE}.sam -nt ${RESULTS_DIR}/${experiment}.oracle.bam ];
then
    CMD="${SCRIPTS_BIN_DIR}/samtools view -@ ${threads} -Sb ${ORACLE}.sam > ${RESULTS_DIR}/${experiment}.oracle_unsorted.bam
${SCRIPTS_BIN_DIR}/samtools sort -@ ${threads} -m 4G -n ${RESULTS_DIR}/${experiment}.oracle_unsorted.bam ${RESULTS_DIR}/${experiment}.oracle
rm ${RESULTS_DIR}/${experiment}.oracle_unsorted.bam"
fi
