unset CMD
#-tc ${threads}

if [ ! -f ${RESULTS_DIR}/${experiment}.gsi.gz ] || [ ${READS} -nt ${RESULTS_DIR}/${experiment}.gsi.gz ];
then
    CMD="${BASE}/mappers/bin/razers3 -v -tc 0 -rr 100 -i ${percid} -m 10000000 -ds -o ${RESULTS_DIR}/${experiment}.gold_unprep.sam ${REFERENCE} ${READS}
${BASE}/scripts/bin/rabema_prepare_sam -i ${RESULTS_DIR}/${experiment}.gold_unprep.sam -o ${RESULTS_DIR}/${experiment}.gold_prep.sam
${SCRIPTS_BIN_DIR}/samtools view -@ ${threads} -Sb ${RESULTS_DIR}/${experiment}.gold_prep.sam > ${RESULTS_DIR}/${experiment}.gold_by_qname.bam
${SCRIPTS_BIN_DIR}/samtools sort -@ ${threads} ${RESULTS_DIR}/${experiment}.gold_by_qname.bam ${RESULTS_DIR}/${experiment}.gold_by_coord
${BASE}/scripts/bin/rabema_build_gold_standard -e ${errors} -o ${RESULTS_DIR}/${experiment}.gsi.gz -r ${REFERENCE} -b ${RESULTS_DIR}/${experiment}.gold_by_coord.bam"
rm ${RESULTS_DIR}/${experiment}.gold_unprep.sam ${RESULTS_DIR}/${experiment}.gold_prep.sam ${RESULTS_DIR}/${experiment}.gold_by_qname.bam ${RESULTS_DIR}/${experiment}.gold_by_coord.bam"
fi
