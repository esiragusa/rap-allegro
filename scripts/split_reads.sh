#!/bin/bash

# create read file chunks if required.
if [ ! -f "${READS}" ] || [ ! -f ${READS}.d/000 ] || [ ${READS} -nt ${READS}.d/000 ]
then
    CMD="dont_measure mkdir -p ${READS}.d; split -a 3 -d -l ${chunk_size} ${READS} ${READS}.d/"
fi

if [ -n "${READS2}" ]
then
    if [ ! -f "${READS2}" ] || [ ! -f ${READS2}.d/000 ] || [ ${READS2} -nt ${READS2}.d/000 ]
    then
        CMD+="
dont_measure mkdir -p ${READS2}.d; split -a 3 -d -l ${chunk_size} ${READS2} ${READS2}.d/"
    fi
fi

