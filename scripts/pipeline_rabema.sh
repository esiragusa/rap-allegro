LOCAL_STORAGE=/export/local-1/public/explab
EXPERIMENT_TYPE=rabema

# predefined job settings
PBS_LIGHT=\
"#PBS -l nodes=1:ppn=1
#PBS -q batch"

PBS_SINGLE=\
"#PBS -l nodes=1:ppn=1"

function gen_steps
{
	REFERENCE="${REFERENCES_DIR}/${organism}.fasta"
    ORACLE="${READS_DIR}/${oracle}"
	INDEX="${INDICES_DIR}/${organism}"
	READS="${READS_DIR}/${reads}"
    unset READS2
    if [ -n "${reads2}" ]; then
            # paired-end mode?
            READS2="${READS_DIR}/${reads2}"
    fi

	exp_init

    unset all_mappers
    for mode in ${modes}
    do
        all_mappers+="${mode2mappers[$mode]} "
    done

    # prepare indices
    for step in ${steps}
    do
        # job id must be reset manually
        EXEC_COLNAMES="\tstep\tmode"
        case "${step}" in

            rabema_gold)
                # Build Rabema gold standard.
                new_step 2 ${step}
                mode=rabema_gold
                EXEC_VALUES="\t${step}\t${mode}"
                SUBSTEP_NAME=".rabema_gold"
                # Build Rabema gold standard.
                if [ ! -n "${READS2}" ]; then
                    . ${SCRIPTS_DIR}/rabema_build_gold_standard.sh
                    append_job rabema_build_gold
                fi
                finish_step "#PBS -l mem=70gb" "#PBS -l nodes=1:ppn=1"
                ;;

            oracle)
                # Build Rabema oracle.
                new_step 3 ${step}
                mode=rabema_oracle
                EXEC_VALUES="\t${step}\t${mode}"
                SUBSTEP_NAME=".oracle"
                . ${SCRIPTS_DIR}/oracle.sh
                append_job build_oracle
                finish_step "#PBS -l mem=70gb" "#PBS -l nodes=1:ppn=1"
                ;;

            map)
                # map reads
                new_step 4 ${step}
                for mode in ${modes}
                do
                    mappers="${mode2mappers[$mode]}"
                    SUBSTEP_NAME=".${mode}"
                    EXEC_VALUES="\t${step}\t${mode}"
                    function work
                    {
                        RESULT=${RESULTS_DIR}/${experiment}.${mode}.${mapper}.bam
                        map_cmd
                    }
                    append_jobs
                    unset -f work
                done
                finish_step
                ;;

            sort)
                # Sort mapping results.
                new_step 5 ${step}
                for mode in ${modes}
                do
                    mappers="${mode2mappers[$mode]}"
                    SUBSTEP_NAME=".${mode}"
                    EXEC_VALUES="\t${step}\t${mode}"
                    function work
                    {
                        unset CMD
                        PREFIX=${RESULTS_DIR}/${experiment}.${mode}.${mapper}
                        . ${SCRIPTS_DIR}/sampe2se.sh
                        . ${SCRIPTS_DIR}/bam_sort.sh
                        unset PREFIX
                    }
                    append_jobs
                    unset -f work
                done
                finish_step "#PBS -l mem=4gb" "#PBS -l nodes=1:ppn=1"
                ;;

            accuracy)
                new_step 6 ${step}
                for mode in ${modes}
                do
                    for accuracy_mode in mapq #errors
                    do
                        mappers="${mode2mappers[$mode]}"
                        SUBSTEP_NAME=".${mode}.${accuracy_mode}"
                        EXEC_COLNAMES="\tstep\tmode\taccuracy-mode"
                        EXEC_VALUES="\t${step}\t${mode}\t${accuracy_mode}"
                        function work
                        {
                            RESULT_PREFIX=${RESULTS_DIR}/${experiment}.${mode}.${mapper}
                            . ${SCRIPTS_DIR}/accuracy.sh
                        }
                        append_jobs
                        unset -f work
                    done
                done
                finish_step "#PBS -l mem=4gb" "#PBS -l nodes=1:ppn=1"
                ;;

            rabema_evaluate)
                # Evaluate the mapping results with Rabema.
                new_step 7 ${step}
                for mode in ${modes}
                do
                    rabema_mode="${mode2rabema[$mode]}"
                    mappers="${mode2mappers[$mode]}"
                    SUBSTEP_NAME=".${mode}.${rabema_mode}"
                    EXEC_COLNAMES="\tstep\tmode\trabema-mode"
                    EXEC_VALUES="\t${step}\t${mode}\t${rabema_mode}"
                    function work
                    {
                        RESULT_PREFIX=${RESULTS_DIR}/${experiment}.${mode}.${mapper}
                        . ${SCRIPTS_DIR}/rabema_evaluate.sh
                    }
                    append_jobs
                    unset -f work
                done
                finish_step "#PBS -l mem=4gb" "#PBS -l nodes=1:ppn=1"
                ;;
        esac
    done
}

