#!/bin/bash

# extract X0 for mapped reads

samtools view -@8 $1 | 
awk '{ if ($3 != "*") { print $13 } }' |
cut -d ':' -f 3 |
awk '{
    m = log($1)/log(2);
    if (m > 10) { m = 10; }
    count[m]++;
} END {
  for (m=0; m < 10; ++m)
    printf("%d\t(<= %d #)\n", count[m], 2^m);   
}'
