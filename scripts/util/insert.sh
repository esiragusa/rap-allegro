#!/bin/sh

if [ $# -eq 0 ]
then
    echo "usage: $0 gold.sam"
    exit 1
fi


awk '{ if ($3 != "*") { total += sqrt(($9)^2); count++ } } END { printf("%g\n", total / count) }' $1

