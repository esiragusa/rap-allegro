#!/bin/bash

# extract NM and X0 for mapped reads
# compute mean, variance, and 95% CI by errors

samtools view -@8 $1 | 
awk '{ if ($3 != "*") { print $12 ":" $13 } }' |
cut -d ':' -f 3,6 |
awk -F ':' '{ print $1, $2 }' |
awk '{
  k = $1;
  p = 1/$2;
  mean[k] += p;
  var[k] += (1 - p) * p;
  count[k]++;
} END {
  for (k=0; i < length(count); ++k)
  {
    mean_est = mean[k] / count[k];
    var_est = var[k] / count[k];
    ci_95 = 1.96 * sqrt((mean_est * (1 - mean_est)) / count[k]);
    printf("<%d,%d>\t%g +- %g\t(%g)\n", k, count[k], 100 * mean_est, 100 * ci_95, 100 * var_est);
  }
}'
