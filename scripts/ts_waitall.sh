#!/bin/bash
SCRIPTS_DIR=$(dirname $0)
TS=${SCRIPTS_DIR}/bin/ts

# Wait for all jobs in the task spooler.

for x in `${TS} | tail -n +2 | awk '{print $1}'`; do
    ${TS} -w ${x}
done
exit 0
