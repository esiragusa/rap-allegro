#!/bin/bash

#
# shared_to_local SHARED_BASE LOCAL_BASE SHARED_FILES
#

SHARED_BASE=$1
LOCAL_BASE=$2
ARGS=("$@")

for (( i=2; i<${#ARGS[@]}; i++ ));
do
    SHARED_FILE=${ARGS[$i]}
    if [ -a ${SHARED_FILE} ]
    then
	# sync with attributes and dereference symlinks
        COMMAND="rsync -aL ${SHARED_FILE} $(dirname ${LOCAL_BASE}${SHARED_FILE#${SHARED_BASE}})/"
        echo $COMMAND
        $COMMAND
    fi
done

