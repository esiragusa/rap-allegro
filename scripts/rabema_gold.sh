function rabema_gold
{
    REFERENCE="${REFERENCES_DIR}/${organism}.fasta"
    READS="${READS_DIR}/${reads}"
    TMP_DIR=${RESULTS_DIR}/${experiment}.gold
    STRATA="-dr 0"

    lines=$((4 * ${num_reads} / ${threads}))

    mkdir -p ${TMP_DIR}

    zcat ${READS} | split -l ${lines} --numeric-suffixes=1 --suffix-length=1 --additional-suffix=.fastq - ${TMP_DIR}/${experiment}.

    for i in $(seq 1 ${threads});
    do
        ${BASE}/mappers/bin/razers3 -v -tc 0 -rr 100 -i ${percid} -m 10000000 ${STRATA} -ds \
            -o ${TMP_DIR}/${experiment}.gold_unprep.$i.bam ${REFERENCE} ${TMP_DIR}/${experiment}.$i.fastq &>> ${TMP_DIR}/${experiment}.$i.log &
    done
    wait

    for i in $(seq 1 ${threads});
    do
        ${BASE}/scripts/bin/rabema_prepare_sam -i ${TMP_DIR}/${experiment}.gold_unprep.$i.bam \
            -o ${TMP_DIR}/${experiment}.gold_prep.$i.bam &>> ${TMP_DIR}/${experiment}.$i.log

        ${SCRIPTS_BIN_DIR}/samtools sort -@ ${threads} ${TMP_DIR}/${experiment}.gold_prep.$i.bam \
            ${TMP_DIR}/${experiment}.gold_by_coord.$i &>> ${TMP_DIR}/${experiment}.$i.log
    done

    for i in $(seq 1 ${threads});
    do
        ${BASE}/scripts/bin/rabema_build_gold_standard -e ${errors} -o ${TMP_DIR}/${experiment}.$i.gsi.gz \
            -r ${REFERENCE} -b ${TMP_DIR}/${experiment}.gold_by_coord.$i.bam &>> ${TMP_DIR}/${experiment}.$i.log &
    done
    wait

    zcat ${TMP_DIR}/${experiment}.1.gsi.gz > ${TMP_DIR}/${experiment}.gsi
    for i in $(seq 2 ${threads});
    do
        zcat ${TMP_DIR}/${experiment}.$i.gsi.gz | tail -n +4 >> ${TMP_DIR}/${experiment}.gsi
    done

    pigz -p 8 -c ${TMP_DIR}/${experiment}.gsi > ${RESULTS_DIR}/${experiment}.gsi.gz

    rm -r ${TMP_DIR}
}