#!/bin/bash
#PBS -N masai-benchmark
#PBS -d /home/esiragusa/NO_BACKUP/explab/
#PBS -j oe
#PBS -l walltime=48:00:00
#PBS -l nodes=1:ppn=24:Intel.X5650
#PBS -l mem=70gb
#PBS -q long

hostname
date
df -h
echo
sleep 2d
