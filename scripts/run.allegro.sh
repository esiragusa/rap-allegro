#!/bin/bash

# ./run.allegro.sh
#
# run batch script remotely

REMOTE_HOST=allegro
REMOTE_BASE=/data/scratch/NO_BACKUP/weese/explab
LOCAL_BASE=/export/userdata/weese/explab
SCRIPTS_DIR=${LOCAL_BASE}/scripts

if [ "$(hostname)" == "${REMOTE_HOST}" ]
then

	for script in ./.*.qsub_all
	do
		echo ${script}
		${script}
	done

else

	# rebase current dir to remote base
	target=$(pwd)
	source=${LOCAL_BASE}
	common_part=$source
	back=
	while [ "${target#$common_part}" = "${target}" ]; do
	    common_part=$(dirname $common_part)
	    back="../${back}"
	done

	EXP_DIR=${back}${target#$common_part/}

	# sync local to cluster
	${SCRIPTS_DIR}/to_allegro.sh

	# submit job(s) on cluster
	ssh ${REMOTE_HOST} "cd ${REMOTE_BASE}/${EXP_DIR}; ./.*.qsub_all"

	# we need something to wait for completion
	#${SCRIPTS_DIR}/from_allegro.sh

fi
