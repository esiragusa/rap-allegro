java="java -Xmx12g -jar"
gatk=${SCRIPTS_BIN_DIR}/GenomeAnalysisTK.jar

if [ ${caller} = 'hc' ]
then
    CMD+="
${java} ${gatk} -nct 8 -T HaplotypeCaller -R ${REFERENCE} -I ${PREFIX}.bam -o ${PREFIX}.${caller}.vcf"
else
    CMD+="
${java} ${gatk} -nct 2 -nt 4 -T UnifiedGenotyper -R ${REFERENCE} -I ${PREFIX}.bam -o ${PREFIX}.${caller}.vcf -glm BOTH"
fi

CMD+="
dont_measure rm ${PREFIX}.${caller}.vcf.idx"

