num_lines=$(echo "${num_reads}*4"|bc)

if [ -n "${READS}" ]
then
   if [ ${READS_DIR}/${reads_base}_1.fastq.gz -nt ${READS} ]
   then
       CMD+="
             dont_measure zcat ${READS_DIR}/${reads_base}_1.fastq.gz | head -n ${num_lines} > ${READS}"
   fi
fi

if [ -n "${READS2}" ]
then
    if [ ${READS_DIR}/${reads_base}_2.fastq.gz -nt ${READS2} ]
    then
        CMD+="
              dont_measure zcat ${READS_DIR}/${reads_base}_2.fastq.gz | head -n ${num_lines} > ${READS2}"
    fi

    if [ ${READS_DIR}/${reads_base}_1.fastq.gz.isize -nt ${READS}.isize ]
    then
        CMD+="
              dont_measure cp ${READS_DIR}/${reads_base}_1.fastq.gz.isize ${READS}.isize"
    fi
fi

