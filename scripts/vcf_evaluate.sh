java="java -Xmx12g -jar"
vcfcomp=${SCRIPTS_BIN_DIR}/USeq/Apps/VCFComparator
vcfprime=${SCRIPTS_BIN_DIR}/vcfallelicprimitives
vcftools=${SCRIPTS_BIN_DIR}/vcftools

CMD+="
${vcfprime} -t complex ${PREFIX}.vcf > ${PREFIX}.prim.vcf
${vcftools} --vcf ${PREFIX}.prim.vcf --remove-indels --recode --recode-INFO-all --stdout > ${PREFIX}.snp.prim.vcf
${vcftools} --vcf ${PREFIX}.prim.vcf --keep-only-indels --recode --recode-INFO-all --stdout > ${PREFIX}.indel.prim.vcf
${java} ${vcfcomp} -a ${REF_VCF} -b ${REF_BED} -c ${PREFIX}.snp.prim.vcf -d ${READS_BED} -s | tail -n +29 | head -n -4 > ${PREFIX}.snp.tsv
${java} ${vcfcomp} -a ${REF_VCF} -b ${REF_BED} -c ${PREFIX}.indel.prim.vcf -d ${READS_BED} -n | tail -n +29 | head -n -4 > ${PREFIX}.indel.tsv
dont_measure rm ${PREFIX}.snp.prim.vcf ${PREFIX}.indel.prim.vcf"
