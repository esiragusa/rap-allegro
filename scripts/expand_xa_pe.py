#!/usr/bin/env python
"""Expansion of XA tag for Paired-End reads.

Note that we may create duplicate copies but that's OK.  We only use this in
our pipeline and our later tools need this.
"""
from __future__ import with_statement

import sys

# Whether or not to fix Hobbes output heuristically.
FIX_HOBBES=True

FLAG_MULTIPLE=0x01
FLAG_FIRST=0x40
FLAG_LAST=0x80
FLAG_RC=0x10
FLAG_SECONDARY=0x100

def revComp(c):
  return {'C': 'A', 'G': 'T', 'A': 'C', 'T': 'G',
          'c': 'a', 'g': 't', 'a': 'c', 't': 'g'}.get(c, c)

def fixLine(line):
    """Clean trailing '*' if a tag column (index >= 11).  This is required for
    Hobbes' broken output.

    Also try to recognize "XA:Z:" tags that are not labeled so."""
    if FIX_HOBBES and not line.startswith('@'):
      _cols = line.split('\t')
      if len(_cols) > 11 and _cols[-1] == '*':
        _cols.pop()
      for i, x in enumerate(_cols):
        if i < 11:
          continue
        if len(x) > 3 and x[2] != ':':  # Guess that this is XA:Z.
          _cols[i] = 'XA:Z:' + _cols[i]
          break
      line = '\t'.join(_cols)
    return line

def fixFlag(flagStr, idStr):
    if idStr[-2] != '/':
        return idStr
    flag = int(flagStr)
    # Assumption: Marker with /1, /2.
    if flagStr[-1] == '1':
        flag = flag | FLAG_MULTIPLE
        flag = flag & (~FLAG_LAST)
        flag = flag | FLAG_FIRST
        templateL[0] = str(flag)
    else:
        flag = flag | FLAG_MULTIPLE
        flag = flag & (~FLAG_FIRST)
        flag = flag | FLAG_LAST
    return str(flag)

def run(fin, fout, copy_seq=False):
  while True:
    l = fin.readline().strip()
    if not l:
      break
    if l.startswith('@'):  # Handle case of header.
      print >>fout, l
      continue
    # Read lines and fix.
    lineL = fixLine(l.strip())
    lineR = fixLine(fin.readline().strip())
    # Print original lines.
    print >>fout, lineL
    print >>fout, lineR

    fieldsL = lineL.split('\t')
    fieldsR = lineR.split('\t')
    for i, field in enumerate(fieldsL):
      if field.startswith('XA:Z:'):
        if not fieldsR[i].startswith('XA:Z:'):  # Handle asymmetric case.
          print >>sys.stderr, 'ERROR: No XA:Z field found in right line "%s".' % lineR
          sys.exit(1)
        # Cut out XA tag for template.
        templateL = fieldsL[0:i] + fieldsL[i+1:]
        templateR = fieldsR[0:i] + fieldsR[i+1:]
        # Try to fix the first/last flag using the read identifier.
        templateL[1] = fixFlag(templateL[1], templateL[0])
        templateR[1] = fixFlag(templateR[1], templateR[0])
        # Copy out and split XA tag.
        entriesL = field[5:-1].split(';')
        entriesR = fieldsR[i][5:-1].split(';')
        if not entriesL[-1]:  # Remove trailing field if terminated with ';'
          entriesL = entriesL[:-1]
        if not entriesR[-1]:  # ibidem
          entriesR = entriesR[:-1]
        for i, entryL in enumerate(entriesL):
          xsL = entryL.split(',')
          xsR = entriesR[i].split(',')
          rowL = list(templateL[:11])  # copy, without tags
          rowR = list(templateR[:11])
          #print row
          rowL[2] = xsL[0]  # chromosome
          rowR[2] = xsR[0]
          rowL[3] = xsL[1]  # pos
          rowR[3] = xsR[1]
          revL = False
          if int(rowL[3]) < 0:
            revL = True
            rowL[3] = str(-int(rowL[3]))
          else:
            rowL[3] = str(int(rowL[3]))
          revR = False
          if int(rowR[3]) < 0:
            revR = True
            rowR[3] = str(-int(rowR[3]))
          else:
            rowR[3] = str(int(rowR[3]))
          # Update flag.
          flagL = int(rowL[1])
          if revL:
            flagL |= FLAG_RC
          elif (flagL & FLAG_RC != 0):
            flagL ^= FLAG_RC
          flagL |= FLAG_SECONDARY
          rowL[1] = str(flagL)
          rowL[5] = xsL[2]  # CIGAR
          flagR = int(rowR[1])
          if revR:
            flagR |= FLAG_RC
          elif (flagR & FLAG_RC != 0):
            flagR ^= FLAG_RC
          flagR |= FLAG_SECONDARY
          rowR[1] = str(flagR)
          rowR[5] = xsR[2]  # CIGAR
          # Update RNEXT and PNEXT.
          rowL[6] = {rowL[2]: '='}.get(rowR[2], rowR[2])
          rowL[7] = rowR[3]
          rowR[6] = {rowR[2]: '='}.get(rowL[2], rowL[2])
          rowR[7] = rowL[3]
          # Remove sequence and quality for secondary records.
          if copy_seq:
            rowL[9] = templateL[9]
            rowR[9] = templateR[9]
            rowL[10] = templateL[10]
            rowR[10] = templateR[10]
            if (int(templateL[1]) & FLAG_RC) != (int(rowL[1]) & FLAG_RC):
              # Reverse-complement read sequence, reverse qualities.
              rowL[9] = ''.join(map(revComp, rowL[9][::-1]))
              rowL[10] = rowL[10][::-1]
            if (int(templateR[1]) & FLAG_RC) != (int(rowR[1]) & FLAG_RC):
              # Reverse-complement read sequence, reverse qualities.
              rowR[9] = ''.join(map(revComp, rowR[9][::-1]))
              rowR[10] = rowR[10][::-1]
          else:
            rowL[9] = '*'
            rowL[10] = '*'
            rowR[9] = '*'
            rowR[10] = '*'
          # Append NM tag.
          rowL.append('NM:i:%d' % int(xsL[3]))
          rowR.append('NM:i:%d' % int(xsR[3]))
          # Strip away all tags.
          # edit distance is ignored
          print >>fout, '\t'.join(rowL)
          print >>fout, '\t'.join(rowR)

def main(args):
  copy_seq = False
  # USAGE: expand_xa.py <IN.sam >OUT.sam
  # USAGE: expand_xa.py IN.sam OUT.sam [--copy-seq]
  if len(args) not in [3, 4]:
    run(sys.stdin, sys.stdout)
    return 0

  if len(args) > 3:
    if args[-1] != '--copy-seq':
      print >>sys.stderr, 'ERROR: Unknown parameter %s' % args[-1]
      return 1
    copy_seq = True
  with open(args[1], 'r') as fin:
    with open(args[2], 'w') as fout:
      run(fin, fout, copy_seq)
  return 0


if __name__ == '__main__':
  sys.exit(main(sys.argv))

