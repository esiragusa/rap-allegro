
# common experiment functions for the generation of jobs and batches
#
# this script requires the following variables to be set:
#   BASE
#   EXP_DIR
#
# the defined functions make use of:
#   experiment
#   mappers

umask 002

# base folders
MAPPERS_DIR=${BASE}/mappers
MAPPERS_BIN_DIR=${MAPPERS_DIR}/bin
SCRIPTS_DIR=${BASE}/scripts
SCRIPTS_BIN_DIR=${SCRIPTS_DIR}/bin
REFERENCES_DIR=${BASE}/references
INDICES_DIR=${BASE}/indices
READS_DIR=${BASE}/reads

# experiment subfolders
RESULTS_DIR=${EXP_DIR}/results
LOGS_DIR=${EXP_DIR}/logs
RESOURCES_DIR=${EXP_DIR}/resources

function exp_init
{
    mkdir -p ${RESULTS_DIR} ${LOGS_DIR} ${RESOURCES_DIR}

    echo "#/bin/bash" > .${experiment}.qsub_all
    echo "#/bin/bash" > .${experiment}.run_local
    chmod -f a+x .${experiment}.qsub_all .${experiment}.run_local
    unset DEPEND_PREV
    output_format="sam"
}

function new_step
{
    #
    # new_step STEP_ID STEP_NAME
    #
    # initializes a new step
    #
    STEP_ID=$1
    STEP_NAME=$2
    unset SUBSTEP_NAME

    rm -f ${EXP_DIR}/.${experiment}.${STEP_ID}.[0-9]*.sh
    JOB_ID=0
}

function finish_step
{
    #
    # finish_step [PBS_LINE]*
    #
    # generate a pbs queue file for the current step
    # (required for the array job execution)
    #

    unset SUBSTEP_NAME
    PBS_JOBID=${experiment}.${STEP_ID}
    PREFIX=${EXP_DIR}/.${PBS_JOBID}
    QSUB_ALL=${EXP_DIR}/.${experiment}.qsub_all
    RUN_LOCAL=${EXP_DIR}/.${experiment}.run_local

    # Build PBS lines at top of script from user input.
    unset PBS_LINES
    for arg in "$@"
    do
        PBS_LINES+="${arg}
"
    done
    # Add default PBS lines.
    if [[ "${PBS_LINES}" != *PBS\ -N* ]]; then
      PBS_LINES+="#PBS -N ${PBS_JOBID}
"
    fi
    if [[ "${PBS_LINES}" != *PBS\ -d* ]]; then
      PBS_LINES+="#PBS -d $(pwd)
"
    fi
    if [[ "${PBS_LINES}" != *PBS\ -j* ]]; then
      PBS_LINES+="#PBS -j oe
"
    fi
    if [[ "${PBS_LINES}" != *PBS\ -l\ walltime* ]]; then
      PBS_LINES+="#PBS -l walltime=20:00:00
"
    fi
    if [[ "${PBS_LINES}" != *PBS\ -l\ nodes* ]]; then
      PBS_LINES+="#PBS -l nodes=1:ppn=24:Intel.X5650
"
    fi
#    if [[ "${PBS_LINES}" != *PBS\ -l\ mem* ]]; then
#      PBS_LINES+="#PBS -l mem=70gb
#"
#    fi
    if [[ "${PBS_LINES}" != *PBS\ -q* ]]; then
      PBS_LINES+="#PBS -q long
"
    fi

    rm -f ${PREFIX}.sh
    # nothing to do?
    if [ "${JOB_ID}" == 0 ]; then
        return
    fi

    # generate PBS array job file
    cat <<EOF >> ${PREFIX}.sh
#!/bin/bash
${PBS_LINES}
hostname
date
free -m
df -h
echo
${PREFIX}.\${PBS_ARRAYID}.sh
EOF
    chmod -f a+x ${PREFIX}.sh

    # add new enqueue entry to %.qsub_all
    echo "J=\$(qsub -t 1-${JOB_ID} ${DEPEND_PREV} ${PREFIX}.sh)" >> ${QSUB_ALL}
    echo "J=\${J%%.*};echo \$J" >> ${QSUB_ALL}
    echo "sleep 2" >> ${QSUB_ALL}
    DEPEND_PREV="-W depend=afterok"
    for i in $(seq ${JOB_ID})
    do
        DEPEND_PREV+=":\$J-$i"
    done
    echo >> ${QSUB_ALL}

    # add new entry to %.run_local
    cat <<EOF >> ${RUN_LOCAL}
for i in \$(seq ${JOB_ID}); do
    PBS_ARRAYID=\$i ${PREFIX}.sh
done
EOF
}

function append_job
{
    #
    # append_job JOB_NAME
    #
    # append a single job consisting of linebreak-separated commands in CMD 
    #

    if [ "${CMD}" == "" ]; then
        return
    fi

    # generate new job file
    job_name="$1"
    JOB_ID=$((${JOB_ID}+1))
    JOB_FILE=${EXP_DIR}/.${experiment}.${STEP_ID}.${JOB_ID}.sh

    UNIQUE_JOB_NAME="${experiment}.${STEP_NAME}${SUBSTEP_NAME}.${job_name}"
    LOG="${LOGS_DIR}/${UNIQUE_JOB_NAME}.log"
    RES="${RESOURCES_DIR}/${UNIQUE_JOB_NAME}.res.tsv"

    echo "# ${STEP_NAME} ${job_name}" > ${JOB_FILE}
    echo "echo ${STEP_NAME} ${job_name} ${experiment}" >> ${JOB_FILE}
    echo "echo -e \"\\n##########################################################\" | tee -a ${LOG}" >> ${JOB_FILE}
    echo "echo -e \"# \$(date) \$(hostname)\" | tee -a ${LOG}" >> ${JOB_FILE}
    echo "echo -e \"# job id:   \${PBS_JOBID}\\n# job name: \${PBS_JOBNAME}\\n\" | tee -a ${LOG}" >> ${JOB_FILE}
    echo >> ${JOB_FILE}
    echo "rm -f ${RES}" >> ${JOB_FILE}
    echo >> ${JOB_FILE}
    IFS='
'
    # append each command
    for cmd in $CMD
    do
        unset WATCH_NAME
        watch_name_prefix=""
        if [[ "$cmd" == *measure_memory* ]]; then
            # If the command includes memory_measure then we set the variable
            # WATCH_NAME which triggers watching of this program in exec.sh
            # instead of the PID.
            WATCH_NAME=$(echo ${cmd#measure_memory=} | awk '{print $1}')
            cmd=${cmd//measure_memory=${WATCH_NAME}}
            watch_name_prefix="WATCH_NAME=${WATCH_NAME} "
        fi

		if [[ "$cmd" == *\>* ]]; then 
            # redirect only the error string if the command contains a redirection 
            suffix="2>> ${LOG}" 
        else 
            suffix="&>> ${LOG}" 
        fi 

        if [[ "$cmd" == *dont_measure* ]]; then
            # don't call exec.sh script if command includes "dont_measure"
            cmd=${cmd//dont_measure/}
            unset prefix
        else
            prefix="${watch_name_prefix}RESOURCES=${RES} EXEC_COLNAMES=\"${EXEC_COLNAMES}\" EXEC_VALUES=\"${EXEC_VALUES}\" ${SCRIPTS_DIR}/exec.sh "
        fi
        cmd=$(echo ${cmd}|sed 's/^[ \t]*//g')
        if [ -z "${cmd}" ]; then
           continue
        fi

        echo "echo '${cmd}' | tee -a ${LOG}" >> ${JOB_FILE}
        if [ -n "${prefix}" ]
        then
            echo "${prefix}\\" >> ${JOB_FILE}
        fi
        echo "      ${cmd} ${suffix}" >> ${JOB_FILE}
        echo >> ${JOB_FILE}
        unset WATCH_NAME
    done
    unset IFS
    echo "echo -e \"DONE\\n\" | tee -a ${LOG}" >> ${JOB_FILE}
    echo >> ${JOB_FILE}
    chmod -f a+x ${JOB_FILE}
}


function append_jobs
{
    #
    # append_jobs
    #
    # append jobs for a single step
    # 1. call function work for each read mapper in ${mappers}
    # 2. function work defines a set of linebreak-separated commands in CMD
    # 3. create job files and fill in the necessary commands
    #

    unset PREV_CMDS

    for mapper in ${mappers}
    do
        # get required commands
        unset CMD
        work

        # check for duplicate commands
        HASH=$(echo ${CMD}|sed 's/[ \t]*//g'|md5sum|awk '{print $1}')
        if [[ "${PREV_CMDS}" == *${HASH}* ]]; then
            continue
        fi
        PREV_CMDS+=" $HASH"
        
        append_job ${mapper}
    done
}



#
# misc functions
#

function absolutesToRelative
{
	#
	# absolutesToRelative TARGET SOURCE
	#
	# determine relative path from SOURCE to TARGET
	# and create link to reference in index folder
	#
	
	target=$1
	source=$2
	common_part=$source
	back=
	while [ "${target#$common_part}" = "${target}" ]; do
		common_part=$(dirname $common_part)
		back="../${back}"
	done
	RELATIVE=${back}${target#$common_part/}
}



#
# rebase files and vars to local disks
#

function save_vars
{
    # save current values
    ORG_REFERENCE="${REFERENCE}"
	ORG_INDEX="${INDEX}"
	ORG_READS="${READS}"
	ORG_READS2="${READS2}"
	ORG_RESULT="${RESULT}"
}

function restore_vars
{
	REFERENCE="${ORG_REFERENCE}"
	INDEX="${ORG_INDEX}"
	READS="${ORG_READS}"
	READS2="${ORG_READS2}"
	RESULT="${ORG_RESULT}"
}

function rebase_vars
{
    #
    # rebase_vars [none|input-only|all] [split]
    #
    # copy input/output files to/from local disks
    #
    # none ......... work on shared fs
    # input-only ... copy only input files, i.e. reads+reference+index, 
    #                result file remains on shared fs
    # all .......... copy all
    #
    # split ........ copy split-reads (reads.d/ instead of reads)



    unset PREPEND
    unset APPEND

    if [ -n ${LOCAL_STORAGE} ] && [[ "$@" != *none* ]]
    then
        # commands to copy files to local fs
        PREPEND="dont_measure rm -rf ${LOCAL_STORAGE}; mkdir -p ${LOCAL_STORAGE}/results;"
        PREPEND2="${SCRIPTS_DIR}/rebase_files.sh ${BASE} ${LOCAL_STORAGE} ${REFERENCE}* ${INDEX}.${mapper} ${INDEX}.${mapper}.*"
        if [[ "$@" == *split* ]]
        then
            PREPEND+="mkdir -p ${LOCAL_STORAGE}${READS#${BASE}}.d;"
            PREPEND2+=" ${READS}.d/[0-9]*"
            if [ -n "${READS2}" ]
            then
                PREPEND+="mkdir -p ${LOCAL_STORAGE}${READS2#${BASE}}.d;"
                PREPEND2+=" ${READS2}.d/[0-9]*"
            fi
        else
            PREPEND2+=" ${READS} ${READS2}"
        fi
        PREPEND+="${PREPEND2}"

        # rebase input paths
        REFERENCE="${LOCAL_STORAGE}${REFERENCE#${BASE}}"
        INDEX="${LOCAL_STORAGE}${INDEX#${BASE}}"
        READS="${LOCAL_STORAGE}${READS#${BASE}}"
        if [ -n "${READS2}" ]
        then
            READS2="${LOCAL_STORAGE}${READS2#${BASE}}"
        fi

        # rebase output paths
        if [[ "$@" == *all* ]]
        then
            RESULT="${LOCAL_STORAGE}${RESULT#${EXP_DIR}}"

            # command to copy files to shared fs
            APPEND="dont_measure ${SCRIPTS_DIR}/rebase_files.sh ${LOCAL_STORAGE} ${EXP_DIR} ${RESULT}; rm -rf ${LOCAL_STORAGE}"
        fi
    fi
}



#
# split mapping of all reads into smaller packages
#

function get_package_suffixes
{
    #
    # get_package_suffixes
    #
    # uses num_lines and chunk_size to compute the suffixes (000, 001, ...)
    # of split read packages
    #

    let "num_chunks=(${num_lines} + ${chunk_size} - 1) / ${chunk_size}"
    suffixes=`seq -w 0 999 | head -n ${num_chunks}`
}

function split_work
{
	#
	# split_work TS_THREADS
	#
	# use commands in CMD generate a list of commands on the 
	# split read packages
	# use task spooler with TS_THREADS many processes
	# simultaneously on one node
	#
	# REQUIREMENTS:
	# CMD must not contain redirections of output/error streams
	# Only the first line in CMD is the split into small packages
	#
	# WARNING: 
	# You can only execute this once at the moment. We could set
	# the environment variable TS_SOCKET to some pid-including/unique path
	# but that would make it impossible to stop services using the ts command.
	
	TS_THREADS=$1

	TS="${SCRIPTS_BIN_DIR}/ts"
	TS_INIT="dont_measure ${TS} -S ${TS_THREADS}; dont_measure trap \"pkill -U \${USER}\" 0 1 2 3 15; unset TS_JIDS;"
	TS_WAIT="${SCRIPTS_DIR}/ts_wait.sh \${TS_JIDS}"
	TS_KILL="dont_measure ${TS} -K;"

	# don't use ts with one thread
	# ###(disabled, as we need the ts_wait entry in the resources file)
	if [ ${TS_THREADS} == 1 ]
	then
		unset TS TS_INIT TS_WAIT TS_KILL
	fi

	UNIQUE_JOB_NAME="${experiment}.${STEP_NAME}${SUBSTEP_NAME}.${mapper}"
    ORG_LOG="${LOGS_DIR}/${UNIQUE_JOB_NAME}.log"
    ORG_RES="${RESOURCES_DIR}/${UNIQUE_JOB_NAME}.ts_res.tsv"

	get_package_suffixes
	
    FIRST_CMD="${CMD%%
*}"
    REMAINDER="${CMD#${FIRST_CMD}}"

	unset TS_CMDS
	for suf in ${suffixes}
	do
		TS_RES="${RESULT}.d/${suf}.res"

		# substitue READS/READS2/RESULT by split files
		TS_CMD=$(echo "${FIRST_CMD}"| sed -e "s#${READS}#${READS}.d/${suf}#" -e "s#${RESULT}#${RESULT}.d/${suf}.sam#")
		if [ -n "${READS2}" ]; then
			TS_CMD=$(echo "${TS_CMD}"| sed "s#${READS2}#${READS2}.d/${suf}#")
		fi

		# append ts call to list
		TS_PREFIX="RESOURCES=${TS_RES} EXEC_MODE=\"new_file\" EXEC_COLNAMES=\"${EXEC_COLNAMES}\" EXEC_VALUES=\"${EXEC_VALUES}\" ${TS} ${SCRIPTS_DIR}/exec.sh"
		
		if [ -n "${TS}" ]
		then
			TS_CMDS+="
					  dont_measure TS_JIDS+=\$(${TS_PREFIX} ${TS_CMD}); TS_JIDS+=\" \""
		else
			TS_CMDS+="
					  dont_measure ${TS_PREFIX} ${TS_CMD}"
		fi
	done
	CMD="dont_measure rm -rf ${RESULT}.d; mkdir -p ${RESULT}.d; ${TS_KILL} ${TS_INIT}
		 ${TS_CMDS}
		 ${TS_WAIT}
		 dont_measure cp ${REFERENCE}.samhead ${ORG_RESULT}; grep -vh '^@' ${RESULT}.d/[0-9][0-9][0-9].sam >> ${ORG_RESULT}
		 dont_measure head -1 ${RESULT}.d/000.res > ${ORG_RES}
		 dont_measure tail -n +2 -q ${RESULT}.d/[0-9][0-9][0-9].res >> ${ORG_RES}
		 dont_measure rm -rf ${RESULT}.d/
		 ${TS_KILL}
		 ${REMAINDER}"
	
	unset APPEND
	if [ -n "${LOCAL_STORAGE}" ]; then
		APPEND="dont_measure rm -rf ${LOCAL_STORAGE}/"
	fi
}


function map_cmd
{
    #
    # map_cmd
    #
    # returns commands for the read mapping step in CMD
    # - the mapper is called on local disks (save_vars,rebase_vars,restore_vars)
    # - hobbes and mrfast are called on small read packages (split_work)
    # - razers3X is RazerS 3 with one thread and parallelized as mrFAST
    #
    # variables used: reads[/reads2], mapper, mode
    #

	if [ -e "${READS}.isize" ]
	then
		. ${READS}.isize
		INSERT_MIN=$((${INSERT_MEAN}-${INSERT_ERR}<0?0:${INSERT_MEAN}-${INSERT_ERR}))
		INSERT_MAX=$((${INSERT_MEAN}+${INSERT_ERR}))
	else
		if [ -n "${READS2}" ]
		then
			echo "ERROR: isize file not found: ${READS}.isize"
		fi
	fi

#	rebase_mode="all"
#	if [[ "${mapper}" == mrfast* ]]
#	then
#		# mrFAST produces output files too large for local disks
#		# therefore we use local disks only for input files (reads, reference, index)
#		rebase_mode="input-only split"
#	fi

	# run mapper on local disks
	save_vars
        rebase_mode="none"
	rebase_vars ${rebase_mode}
	
#	if [[ "${mapper}" == mrfast* ]]
#	then
#		# enqueue different mapper calls to the task spooler (ts)
#		org_threads=${threads}
#		
#        threads=1
#        ts_threads=${org_threads}
#
#		. ${MAPPERS_DIR}/${mapper}.sh
#		split_work ${ts_threads}
#		
#		threads=${org_threads}
#	else
		. ${MAPPERS_DIR}/${mapper}.sh
#	fi

	CMD="${PREPEND}
		 ${CMD}
		 ${APPEND}"						

	restore_vars
}
