#!/usr/bin/env python
from __future__ import with_statement

import sys

# Whether or not to fix Hobbes output heuristically.
FIX_HOBBES=True

FLAG_RC=0x10
FLAG_SECONDARY=0x100

def revComp(c):
  return {'C': 'A', 'G': 'T', 'A': 'C', 'T': 'G',
          'c': 'a', 'g': 't', 'a': 'c', 't': 'g'}.get(c, c)

def run(fin, fout, copy_seq=False):
  for line in fin:
    line = line.strip()
    # Clean trailing '*' if a tag column (index >= 11).  This is required for
    # Hobbes' broken output.
    #
    # Also try to recognize "XA:Z:" tags that are not labeled so.
    if FIX_HOBBES and not line.startswith('@'):
      _cols = line.split('\t')
      if len(_cols) > 11 and _cols[-1] == '*':
        _cols.pop()
      for i, x in enumerate(_cols):
        if i < 11:
          continue
        if len(x) > 3 and x[2] != ':':  # Guess that this is XA:Z.
          _cols[i] = 'XA:Z:' + _cols[i]
          break
      line = '\t'.join(_cols)
    print >>fout, line
    if line.startswith('@'):
      continue
    fields = line.split('\t')
    for i, field in enumerate(fields):
      if field.startswith('XA:Z:'):
        template = fields[0:i] + fields[i+1:]
        entries = field[5:-1].split(';')
        if not entries[-1]:  # Remove trailing field if terminated with ';'
          entries = entries[:-1]
        for entry in entries:
          xs = entry.split(',')
          if len(xs) != 4 or xs[3] == '':
              continue  # Skip if incorrect entry, caused problem with Hobbes.
          row = list(template[:11])  # copy, without tags
          #print row
          row[2] = xs[0]  # chromosome
          row[3] = xs[1]  # pos
          rev = False
          if int(row[3]) < 0:
            rev = True
            row[3] = str(-int(row[3]))
          else:
            row[3] = str(int(row[3]))
          # Update flag.
          flag = int(row[1])
          if rev:
            flag |= FLAG_RC
          elif (flag & FLAG_RC != 0):
            flag ^= FLAG_RC
          flag |= FLAG_SECONDARY
          row[1] = str(flag)
          row[5] = xs[2]  # CIGAR
          # Remove sequence and quality for secondary records.
          if copy_seq:
            row[9] = template[9]
            row[10] = template[10]
            if (int(template[1]) & FLAG_RC) != (int(row[1]) & FLAG_RC):
              # Reverse-complement read sequence, reverse qualities.
              row[9] = ''.join(map(revComp, row[9][::-1]))
              row[10] = row[10][::-1]
          else:
            row[9] = '*'
            row[10] = '*'
          # Append NM tag.
          row.append('NM:i:%d' % int(xs[3]))
          # Strip away all tags.
          # edit distance is ignored
          print >>fout, '\t'.join(row)

def main(args):
  copy_seq = False
  # USAGE: expand_xa.py <IN.sam >OUT.sam
  # USAGE: expand_xa.py IN.sam OUT.sam [--copy-seq]
  if len(args) not in [3, 4]:
    run(sys.stdin, sys.stdout)
    return 0

  if len(args) > 3:
    if args[-1] != '--copy-seq':
      print >>sys.stderr, 'ERROR: Unknown parameter %s' % args[-1]
      return 1
    copy_seq = True
  with open(args[1], 'r') as fin:
    with open(args[2], 'w') as fout:
      run(fin, fout, copy_seq)
  return 0


if __name__ == '__main__':
  sys.exit(main(sys.argv))

