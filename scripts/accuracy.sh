accuracy_options=""
if [ "${accuracy_mode}" == "mapq" ]
then
    accuracy_options="-q"
fi

CMD="${SCRIPTS_DIR}/bin/sameval ${accuracy_options} -d ${RESULTS_DIR}/${experiment}.oracle.bam ${RESULT_PREFIX}.bam --out-tsv ${RESULT_PREFIX}.accuracy.${accuracy_mode}.tsv"