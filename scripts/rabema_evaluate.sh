unset CMD

unset options
case "${mapper}" in
    yara*)
        options="--trust-NM --extra-pos-tag XP"
        ;;
    masai*|razers*)
        options="--trust-NM"
        ;;
    *)
        ;;
esac

CMD="${SCRIPTS_DIR}/bin/rabema_evaluate --DONT-PANIC ${options} -c ${rabema_mode} -e ${errors} -r ${REFERENCE} -g ${RESULTS_DIR}/${experiment}.gsi.gz -b ${RESULT_PREFIX}.bam --out-tsv ${RESULT_PREFIX}.rabema.${rabema_mode}.rabema_report_tsv"

CMD+="
mv ${RESULT_PREFIX}.rabema.${rabema_mode}.rabema_report_tsv ${RESULT_PREFIX}.rabema.${rabema_mode}.tsv"
