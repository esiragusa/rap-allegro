LOCAL_STORAGE=/export/local-1/public/explab

# predefined job settings
PBS_LIGHT=\
"#PBS -l nodes=1:ppn=1
#PBS -q batch"

PBS_SINGLE=\
"#PBS -l nodes=1:ppn=1"

function gen_steps
{
	REFERENCE="${REFERENCES_DIR}/${organism}.fasta"
	INDEX="${INDICES_DIR}/${organism}"
	READS="${READS_DIR}/${reads}"
    unset READS2
    if [ -n "${reads2}" ]; then
            # paired-end mode?
            READS2="${READS_DIR}/${reads2}"
    fi

	exp_init

    unset all_mappers
    for mode in ${modes}
    do
        all_mappers+="${mode2mappers[$mode]} "
    done

	# prepare indices
	for step in ${steps}
	do
		# job id must be reset manually
		EXEC_COLNAMES="\tstep\tmode"
		case "${step}" in

			map)
				# map reads
				new_step 2 ${step}
				for mode in ${modes}
				do
                    mappers="${mode2mappers[$mode]}"
                    SUBSTEP_NAME=".${mode}"
                    EXEC_VALUES="\t${step}\t${mode}"
                    function work
                    {
                        RESULT="${RESULTS_DIR}/${experiment}.${mode}.${mapper}.bam"
                        map_cmd
                    }
                    append_jobs
                    unset -f work
				done
				finish_step
				;;

		   sort)
				# sort reads
				new_step 3 ${step}
				for mode in ${modes}
				do
                    mappers="${mode2mappers[$mode]}"
                    SUBSTEP_NAME=".${mode}"
					EXEC_VALUES="\t${step}\t${mode}"
					function work
					{
                        unset CMD
                        PREFIX="${RESULTS_DIR}/${experiment}.${mode}.${mapper}"
						. ${SCRIPTS_DIR}/bam_sort.sh
						. ${SCRIPTS_DIR}/bam_index.sh
						unset PREFIX
					}
					append_jobs
					unset -f work
				done
				finish_step "${PBS_LIGHT}"
				;;

		   call)
				# perform variant calling
				new_step 4 ${step}
                for mode in ${modes}
                do
                    for caller in ${callers}
                    do
                        mappers="${mode2mappers[$mode]}"
                        SUBSTEP_NAME=".${mode}.${caller}"
                        EXEC_VALUES="\t${step}\t${mode}\t${caller}"
                        function work
                        {
                            unset CMD
                            PREFIX="${RESULTS_DIR}/${experiment}.${mode}.${mapper}"
                            . ${SCRIPTS_DIR}/vcf_call.sh
                            unset PREFIX
                        }
                        append_jobs
                        unset -f work
                    done
                done
				finish_step "${PBS_LIGHT}"
				;;

			evaluate)
				# evaluate calling results
				new_step 5 ${step}
                for mode in ${modes}
                do
                    for caller in ${callers}
                    do
                        mappers="${mode2mappers[$mode]}"
                        SUBSTEP_NAME=".${mode}.${caller}"
                        EXEC_VALUES="\t${step}\t${mode}\t${caller}"
                        function work
                        {
                            unset CMD
                            PREFIX="${RESULTS_DIR}/${experiment}.${mode}.${mapper}.${caller}"
                            REF_VCF="${REFERENCES_DIR}/${organism}.vcf.gz"
                            REF_BED="${REFERENCES_DIR}/${organism}.bed.gz"
                            READS_BED="${READS}.bed"
                            . ${SCRIPTS_DIR}/vcf_evaluate.sh
                            unset PREFIX REF_VCF REF_BED READS_BED
                        }
                        append_jobs
                        unset -f work
                    done
                done
				finish_step "${PBS_LIGHT}"
				;;
		esac
	done
}

