#!/bin/bash

#DIR=$(dirname $0)
#BIN=${DIR}/bin
#${BIN}/fastq-dump -v --gzip --split-files ERR161544

# Extract 10M reads and remove their meta info
zcat ERR161544_1.fastq.gz | head -n 40000000 | awk '{ print $1 }' | pigz -p 8 -c > hiseq_hg19_ERR161544_10M.fastq.gz
