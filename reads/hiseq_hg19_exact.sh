#!/bin/bash

SCRIPTS=$(dirname $0)
BASE=${SCRIPTS}/..
#SCRIPTS=~/Code/seqan-builds/Release-Clang
#BASE=~/Datasets/abra

BIN=${SCRIPTS}/bin
MASON=${BIN}/mason
MASON_VCF=~/Code/seqan-builds/Release-Gcc/bin/mason_variator
MASON_SIM=~/Code/seqan-builds/Release-Gcc/bin/mason_simulator
SAMTOOLS=samtools
BAM_PE2SE=${BIN}/sampe2se

REF_NAME=$1
REF_PATH=${BASE}/references/$REF_NAME.fasta
REF_VCF=${BASE}/references/$REF_NAME.vcf
#REF_PATH=~/Datasets/$REF_NAME/$REF_NAME.fasta
#REF_VCF=~/Datasets/$REF_NAME/$REF_NAME.vcf

# Haplotype params
#HAPLO_SNP_RATE=0.001
#HAPLO_INDEL_RATE=0.0001
#HAPLO_MIN_INDEL=1
#HAPLO_MAX_INDEL=6

# Reads params
NUM_READS=1000000
READ_LENGTH=100

# Quality params
QUAL_BEGIN=40
QUAL_END=40
QUAL_STD_BEGIN=0
QUAL_STD_END=0

# Paired-end params
PAIR_INSERT_MEAN=300
PAIR_INSERT_ERR=30
PAIR_ORIENTATION='FR'

PROB_INS=0.0
PROB_DEL=${PROB_INS}

PROJ=hiseq_${REF_NAME}_exact
RESULT_PREFIX=${BASE}/reads/${PROJ}
RESULT=${RESULT_PREFIX}.fastq
RESULT_1=${RESULT_PREFIX}_1.fastq
RESULT_2=${RESULT_PREFIX}_2.fastq
RESULT_SAM=${RESULT_PREFIX}.sam
RESULT_PE_SAM=${RESULT_PREFIX}_pe.sam
LOG=${RESULT_PREFIX}.log


# Empty log
cat /dev/null > ${LOG}


# Simulate reads with mason2
CMD="${MASON_SIM} --num-threads 8 --seed 43 --embed-read-info --num-fragments ${NUM_READS} \
     --input-reference ${REF_PATH} \
     -o ${RESULT_1} -or ${RESULT_2} -oa ${RESULT_SAM} --read-name-prefix ${PROJ}. \
     --seq-technology illumina --illumina-read-length ${READ_LENGTH} \
     --illumina-prob-mismatch 0.0 --illumina-prob-mismatch-begin 0.0  --illumina-prob-mismatch-end 0.0 \
     --illumina-prob-insert ${PROB_INS} --illumina-prob-deletion ${PROB_DEL} \
     --illumina-quality-mean-begin 40 --illumina-quality-mean-end 40  --illumina-quality-stddev-begin 0.0 --illumina-quality-stddev-end 0.0 \
     --fragment-size-model normal --seq-mate-orientation ${PAIR_ORIENTATION} \
     --fragment-mean-size ${PAIR_INSERT_MEAN} --fragment-size-std-dev ${PAIR_INSERT_ERR}"
echo "EXECUTING "$CMD
echo "${CMD}" >> ${LOG}
${CMD} &>> ${LOG}
mv ${RESULT_PREFIX}.sam ${RESULT_PE_SAM}


# Output template information
echo "INSERT_MEAN=${PAIR_INSERT_MEAN}" > ${RESULT_1}.isize
let INSERT_ERR=$PAIR_INSERT_ERR*3
echo "INSERT_ERR=${INSERT_ERR}" >> ${RESULT_1}.isize


# Remove extra info from fastq
awk '{ print $1 }' ${RESULT_1} > ${RESULT_1}.tmp
awk '{ print $1 }' ${RESULT_2} > ${RESULT_2}.tmp
mv ${RESULT_1}.tmp ${RESULT_1}
mv ${RESULT_2}.tmp ${RESULT_2}


# Transform reads into single-end - concatenate and remove suffixes /1, /2 with prefixes L, R.
sed "s/^@${PROJ}\.\([[:digit:]]*\)\/1/@${PROJ}.L\1/g" ${RESULT_1} >  ${RESULT}
sed "s/^@${PROJ}\.\([[:digit:]]*\)\/2/@${PROJ}.R\1/g" ${RESULT_2} >> ${RESULT}


# Transform reads into single-end
CMD="${BAM_PE2SE} ${RESULT_PE_SAM} ${RESULT_SAM}"
echo "EXECUTING "$CMD
echo "${CMD}" >> ${LOG}
${CMD} &>> ${LOG}


# Output histograms
echo >> ${LOG}
echo "NM HISTOGRAM" >> ${LOG}
for e in $(seq 0 10); do grep NM:i:$e ${RESULT_SAM} | wc -l >> ${LOG}; done;

echo >> ${LOG}
echo "SEQ ERRORS HISTOGRAM" >> ${LOG}
for e in $(seq 0 10); do grep XE:i:$e ${RESULT_SAM} | wc -l >> ${LOG}; done;

echo >> ${LOG}
echo "SNPS HISTOGRAM" >> ${LOG}
for e in $(seq 0 10); do grep XS:i:$e ${RESULT_SAM} | wc -l >> ${LOG}; done;

echo >> ${LOG}
echo "INDELS HISTOGRAM" >> ${LOG}
for e in $(seq 0 10); do grep XI:i:$e ${RESULT_SAM} | wc -l >> ${LOG}; done;


echo
