#!/bin/bash

DIR=$(dirname $0)
BIN=${DIR}/bin

${BIN}/fastq-dump -v --gzip --split-files SRR1611178

wget -O SeqCapEZ_Exome_v3.0.zip http://www.nimblegen.com/downloads/annotation/ez_exome_v3/SeqCapEZ_Exome_v3.0_Design_Annotation_files.zip
unzip SeqCapEZ_Exome_v3.0.zip
mv SeqCap_EZ_Exome_v3_capture.bed SRR1611178.bed
rm SeqCap*
