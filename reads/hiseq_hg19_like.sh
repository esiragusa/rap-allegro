#!/bin/bash

READS=$(dirname $0)
BASE=${READS}/..

BIN=${READS}/bin
MASON_VCF=${BIN}/mason_variator
MASON_SIM=${BIN}/mason_simulator
SAMPE2SE=${BASE}/scripts/bin/sampe2se
SAMTOOLS=samtools

#REF_NAME=$1
REF_NAME=hg19
REF_PATH=${BASE}/references/$REF_NAME.fasta
REF_VCF=${BASE}/references/$REF_NAME.sim.vcf

# Haplotype params
HAPLO_SNP_RATE=0.001
HAPLO_INDEL_RATE=0.0001
HAPLO_MIN_INDEL=1
HAPLO_MAX_INDEL=6

# Reads params
NUM_READS=5000000
READ_LENGTH=100

# Quality params
QUAL_BEGIN=40
QUAL_END=39.5
QUAL_STD_BEGIN=1
QUAL_STD_END=15

# Paired-end params
PAIR_INSERT_MEAN=300
PAIR_INSERT_ERR=30
PAIR_ORIENTATION='FR'

SCALING=0.6
PROB_INS=$(echo "0.0001*${SCALING}" | bc)
PROB_DEL=${PROB_INS}

PROJ=hiseq_${REF_NAME}_like
RESULT_PREFIX=${BASE}/reads/${PROJ}
RESULT=${RESULT_PREFIX}.fastq
RESULT_1=${RESULT_PREFIX}_1.fastq
RESULT_2=${RESULT_PREFIX}_2.fastq
RESULT_SAM=${RESULT_PREFIX}.sam
RESULT_PE_SAM=${RESULT_PREFIX}_pe.sam
LOG=${RESULT_PREFIX}.log


# Empty log
cat /dev/null > ${LOG}


# Simulate haplotype with mason2
if [ ! -f "${REF_VCF}" ];
then
    CMD="${MASON_VCF} --in-reference ${REF_PATH} --out-vcf ${REF_VCF} \
        --num-haplotypes 2 --snp-rate ${HAPLO_SNP_RATE} --small-indel-rate ${HAPLO_INDEL_RATE} \
        --min-small-indel-size ${HAPLO_MIN_INDEL} --max-small-indel-size ${HAPLO_MAX_INDEL} \
        --sv-indel-rate 0.0 --sv-inversion-rate 0.0 --sv-translocation-rate 0.0 --sv-duplication-rate 0.0"

    echo "EXECUTING "$CMD
    echo "${CMD}" >> ${LOG}
    ${CMD} &>> ${LOG}
fi


# Simulate reads with mason2
CMD="${MASON_SIM} --num-threads 8 --seed 0 --embed-read-info --num-fragments ${NUM_READS} \
     --input-reference ${REF_PATH} --input-vcf ${REF_VCF} \
     -o ${RESULT_1} -or ${RESULT_2} -oa ${RESULT_SAM} --read-name-prefix ${PROJ}. \
     --seq-technology illumina --illumina-read-length ${READ_LENGTH} \
     --illumina-prob-mismatch-scale ${SCALING} --illumina-prob-insert ${PROB_INS} --illumina-prob-deletion ${PROB_DEL} \
     --fragment-size-model normal --seq-mate-orientation ${PAIR_ORIENTATION} \
     --fragment-mean-size ${PAIR_INSERT_MEAN} --fragment-size-std-dev ${PAIR_INSERT_ERR}"
echo "EXECUTING "$CMD
echo "${CMD}" >> ${LOG}
${CMD} &>> ${LOG}
mv ${RESULT_PREFIX}.sam ${RESULT_PE_SAM}


# Remove extra info from fastq
awk '{ print $1 }' ${RESULT_1} > ${RESULT_1}.tmp
awk '{ print $1 }' ${RESULT_2} > ${RESULT_2}.tmp
mv ${RESULT_1}.tmp ${RESULT_1}
mv ${RESULT_2}.tmp ${RESULT_2}


# Transform reads into single-end - concatenate and remove suffixes /1, /2 with prefixes L, R.
sed "s/^@${PROJ}\.\([[:digit:]]*\)\/1/@${PROJ}.L\1/g" ${RESULT_1} >  ${RESULT}
sed "s/^@${PROJ}\.\([[:digit:]]*\)\/2/@${PROJ}.R\1/g" ${RESULT_2} >> ${RESULT}


# Transform reads into single-end
CMD="${SAMPE2SE} ${RESULT_PE_SAM} ${RESULT_SAM}"
echo "EXECUTING "$CMD
echo "${CMD}" >> ${LOG}
${CMD} &>> ${LOG}


# Compress reads
pigz -p 8 -c ${RESULT_1} > ${RESULT_1}.gz
pigz -p 8 -c ${RESULT_2} > ${RESULT_2}.gz
pigz -p 8 -c ${RESULT} > ${RESULT}.gz
rm ${RESULT_1} ${RESULT_2} ${RESULT}


# Output template information
#echo "INSERT_MEAN=${PAIR_INSERT_MEAN}" > ${RESULT_1}.gz.isize
#let INSERT_ERR=$PAIR_INSERT_ERR*3
#echo "INSERT_ERR=${INSERT_ERR}" >> ${RESULT_1}.gz.isize


# Output histograms
echo >> ${LOG}
echo "NM HISTOGRAM" >> ${LOG}
for e in $(seq 0 10); do grep NM:i:$e ${RESULT_SAM} | wc -l >> ${LOG}; done;

echo >> ${LOG}
echo "SEQ ERRORS HISTOGRAM" >> ${LOG}
for e in $(seq 0 10); do grep XE:i:$e ${RESULT_SAM} | wc -l >> ${LOG}; done;

echo >> ${LOG}
echo "SNPS HISTOGRAM" >> ${LOG}
for e in $(seq 0 10); do grep XS:i:$e ${RESULT_SAM} | wc -l >> ${LOG}; done;

echo >> ${LOG}
echo "INDELS HISTOGRAM" >> ${LOG}
for e in $(seq 0 10); do grep XI:i:$e ${RESULT_SAM} | wc -l >> ${LOG}; done;


echo
