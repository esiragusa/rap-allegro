#!/bin/bash
# ./run.sh [step [mapper]]
#
# Script to run GCAT experiment.
# step = prepare|map|sort|call|evaluate|all specifies which step should be run (default: all)
# mapper specifies a certain read mapper (default: all)

# set directory variables
BASE=../..
EXP_DIR=$(dirname $0)

. ${BASE}/scripts/functions.sh
. ${BASE}/scripts/pipeline_genotyping.sh

# experiment settings
modes="default"
steps="map sort call evaluate call evaluate"

declare -A mode2mappers
mode2mappers[default]="yaraDev yaraDev_strata bowtie2 bwa_mem" #bwa gem"

read_len=100
erate=0.05
threads=8

errors=$(echo "(${read_len}*${erate})/1"|bc)
percid=$(echo "(1-${erate})*100"|bc)

organism=hg19
# HiSeq
#experiment=gcat_set_025
experiment=SRR1611178
reads=${experiment}_1.fastq.gz
reads2=${experiment}_2.fastq.gz

# IonTorrent
#experiment=gcat_set_036
#reads=${experiment}.fastq.gz
#unset reads2
#unset errors read_len


# select a specific step
if [ "$1" != "" ] && [ "$1" != "all" ]; then
    steps="$1"
fi

# select a specific mapper
if [ "$2" != "" ]; then
    for mode in ${modes}
    do
        if [[ "${mode2mappers[${mode}]}" == *$2* ]]
        then
            mode2mappers[${mode}]="$2"
        else
            mode2mappers[${mode}]=""
        fi
    done
fi

gen_steps
