#!/bin/bash
# ./run.sh [step [mapper]]
#
# Script to run Accuracy experiment.
# step = oracle|map|sort|accuracy|all specifies which step should be run (default: all)
# mapper specifies a certain read mapper (default: all)

# set directory variables
BASE=../..
EXP_DIR=$(dirname $0)

. ${BASE}/scripts/functions.sh
. ${BASE}/scripts/pipeline_rabema.sh

# experiment settings
modes="default"
steps="oracle map sort accuracy"

declare -A mode2mappers
mode2mappers[default]="yaraDev yaraDev_strata bowtie2 bwa gem bwa_mem"

# TEST YARA
steps="map sort accuracy"
mode2mappers[default]="yaraDev_strata bowtie2 gem bwa_mem"

sequencer=hiseq
organism=hg19
reads_base=like
sequencing=se
#sequencing=pe
read_len=100
erate=0.05
threads=8

errors=$(echo "(${read_len}*${erate})/1"|bc)
percid=$(echo "(1-${erate})*100"|bc)

experiment=${sequencer}_${organism}_${reads_base}_${sequencing}
oracle=${sequencer}_${organism}_${reads_base}

if [ "$sequencing" == "se" ]; then
    reads=${sequencer}_${organism}_${reads_base}.fastq.gz
else
    reads=${sequencer}_${organism}_${reads_base}_1.fastq.gz
    reads2=${sequencer}_${organism}_${reads_base}_2.fastq.gz
fi

# select a specific step
if [ "$1" != "" ] && [ "$1" != "all" ]; then
    steps="$1"
fi

# select a specific mapper
if [ "$2" != "" ]; then
    for mode in ${modes}
    do
        if [[ "${mode2mappers[${mode}]}" == *$2* ]]
        then
            mode2mappers[${mode}]="$2"
        else
            mode2mappers[${mode}]=""
        fi
    done
fi

gen_steps
