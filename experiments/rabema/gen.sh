#!/bin/bash
# ./run.sh [step [mapper]]
#
# Script to run Rabema Gold experiment.
# step = prepare|rabema_gold|map|sort|rabema_evaluate|all specifies which step should be run (default: all)
# mapper specifies a certain read mapper (default: all)
#
# You can also set the environment variable ORGA to set the organism to
# generate the scripts for.

# set directory variables
BASE=../..
EXP_DIR=$(dirname $0)

. ${BASE}/scripts/functions.sh
. ${BASE}/scripts/pipeline_rabema.sh

# experiment settings
#modes="best all"
#steps="rabema_gold map sort rabema_evaluate"
modes="all"
steps="map sort rabema_evaluate"

declare -A mode2rabema
mode2rabema[best]="all-best"
mode2rabema[all]="all"

declare -A mode2mappers
#mode2mappers[best]="yaraDev bowtie2 bwa bwa_mem gem"
#mode2mappers[all]="yaraDev razers3 gem hobbes"
mode2mappers[best]="yaraDev"
mode2mappers[all]="yaraDev"

sequencer=hiseq
read_len=100
erate=0.05
threads=8
#num_reads=10000
#reads_suffix=10k
num_reads=1000000
reads_suffix=1M
#num_reads=10000000
#reads_suffix=10M

organism=hg19
reads_base=ERR161544

#sequencer=miseq
#organism=hg19
#reads_base=SRR385938
#read_len=151
#erate=0.04

# experiment variables
errors=$(echo "(${read_len}*${erate})/1"|bc)
percid=$(echo "(1-${erate})*100"|bc)

experiment=${sequencer}_${organism}_${reads_base}_${reads_suffix}
reads=${sequencer}_${organism}_${reads_base}_${reads_suffix}.fastq.gz

# build the gold standard in parallel
#. ${BASE}/scripts/rabema_gold.sh
#rabema_gold

# select a specific step
if [ "$1" != "" ] && [ "$1" != "all" ]; then
    steps="$1"
fi

# select a specific mapper
if [ "$2" != "" ]; then
    for mode in ${modes}
    do
        if [[ "${mode2mappers[${mode}]}" == *$2* ]]
        then
            mode2mappers[${mode}]="$2"
        else
            mode2mappers[${mode}]=""
        fi
    done
fi

gen_steps

