source(file="../../plots/rabema.R")

args <- commandArgs(TRUE)

prefix_headers = ""
prefix_units = "\\footnotesize"
prefix_mappers = ""
show_units = TRUE

digits = 1
max_errors = 5
threads = 8

READ_LENGTHS = c(100)

ORGS = c("hiseq_hg19_ERR161544_10M")
ORGS_EXTRA = c("Illumina HiSeq 2000")
num_reads = 10000000
MODES = c("best")
MODE2MAPPERS = list(
    best = c("yaraDev","gem","bowtie2","bwa")#,"bwa_mem")
)
COLUMNS = c("Rrx_all-best","P_throughput","P_memory")
write_table("hiseq_hg19_ERR161544_10M")

ORGS = c("hiseq_hg19_ERR161544_1M")
ORGS_EXTRA = c("Illumina HiSeq 2000")
num_reads = 1000000
MODES = c("all")
MODE2MAPPERS = list(
    all = c("yaraDev","gem","hobbes","razers3")
)
COLUMNS = c("Rrx_all","P_throughput","P_memory")
write_table("hiseq_hg19_ERR161544_1M")

