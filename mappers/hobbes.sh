unset CMD

# split filename without extension into substrings separated by _
IFS="_"
params=(${mapper%.*})
unset IFS

bin=${params[0]}
#format=${params[1]:=sam}

if [ "${mode}" == "prepare" ]
then

    if [ ${REFERENCE} -nt ${INDEX}.hobbes.index ]
    then
        CMD="${MAPPERS_BIN_DIR}/hobbes-index -p ${threads} -g 11 --sref ${REFERENCE} -i ${INDEX}.hobbes.index"
    fi

else

    output_opts=""
    if [ "${format}" == "native" ]
    then
        output_opts="--nocigar"
    fi

    input_opts="--gzip"
    if [ -z "${READS2}" ]; then
        input_opts+=" -q ${READS}"
    else
        input_opts+=" --pe --seqfq1 ${READS} --seqfq2 ${READS2} --min ${INSERT_MIN} --max ${INSERT_MAX}"
    fi

    CMD="${MAPPERS_BIN_DIR}/hobbes --noprogress -p ${threads} --sref ${REFERENCE} -i ${INDEX}.hobbes.index -a --indel -v ${errors} ${output_opts} ${input_opts} | samtools view -@ ${threads} -bS - > ${RESULT}"
fi
