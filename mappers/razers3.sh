unset CMD

# split filename without extension into substrings separated by _
IFS="_"
params=(${mapper%.*})
unset IFS

# binary
bin=${params[0]}
#format=${params[1]:=sam}

#unset result_suffix
#if [ "${format}" == "native" ]
#then
#    result_suffix=".native"
#fi

CMD="${MAPPERS_BIN_DIR}/${bin} -vv -tc ${threads} -rr 100 -i ${percid}"

#if [ -z "${NO_INDELS}" ]; then
#    CMD+=" --no-gaps"
#fi

case ${mode:=default} in
    all)
        CMD+=" -m 1000000"
        ;;
    best)
        CMD+=" -m 100 -dr 1"
        ;;
    any)
        CMD+=" -m 1 -dr 1"
        ;;
esac

CMD+=" ${REFERENCE} -o ${RESULT}"

# Check whether we want single-end or paired-end mode.
if [ -z "${READS2}" ]
then
    # Single-End
    CMD+=" ${READS}"
else
    # Paired-End
    CMD+=" --library-length ${INSERT_MEAN} --library-error ${INSERT_ERR}"
    CMD+=" ${READS} ${READS2}"
fi

# no preparation required
if [ "${mode}" == "prepare" ]
then
    unset CMD
fi

