unset CMD

if [ "${mode}" == "prepare" ]
then

    if [ ${REFERENCE} -nt ${INDEX}.bwa.bwt ]
    then
        CMD="${MAPPERS_BIN_DIR}/${mapper} index -a bwtsw -p ${INDEX}.bwa ${REFERENCE}"
    fi

else

    unset aln_options
    unset samse_options
    case ${mode:=default} in
        all)
            aln_options="-N"
            samse_options="-n 1000000"
            ;;
        *)
            ;;
    esac

    read_group="'@RG\tID:none\tSM:none'"

    if [ "$mapper" == "bwa_mem" ]
    then
        input="${READS} ${READS2}"
        CMD="${MAPPERS_BIN_DIR}/bwa mem -t ${threads} ${INDEX}.bwa ${input} -R ${read_group} | samtools view -@ ${threads} -bS - > ${RESULT}"

    else # bwa aln

        # Check whether we want single-end or paired-end mode.  Generate command
        # line depending on this.
        if [ -z "${READS2}" ]
        then

            CMD="${MAPPERS_BIN_DIR}/bwa aln ${aln_options} -t ${threads} ${INDEX}.bwa ${READS} > ${RESULT}.sai
                 ${MAPPERS_BIN_DIR}/bwa samse ${samse_options} ${INDEX}.bwa ${RESULT}.sai ${READS} -r ${read_group} | samtools view -@ ${threads} -bS - > ${RESULT}
	         dont_measure rm ${RESULT}.sai"

        else

            if [ "$mapper" != "bwa_sw" ]
            then
                sampe_options="-s" # -a ${INSERT_MAX}
            fi

            CMD="${MAPPERS_BIN_DIR}/bwa aln ${aln_options} -t ${threads} ${INDEX}.bwa ${READS}  > ${RESULT}_1.sai
                 ${MAPPERS_BIN_DIR}/bwa aln ${aln_options} -t ${threads} ${INDEX}.bwa ${READS2} > ${RESULT}_2.sai
                 ${MAPPERS_BIN_DIR}/bwa sampe ${samse_options} ${sampe_options} ${INDEX}.bwa ${RESULT}_1.sai ${RESULT}_2.sai ${READS} ${READS2} -r ${read_group} | samtools view -@ ${threads} -bS - > ${RESULT}
                 dont_measure rm ${RESULT}_1.sai ${RESULT}_2.sai"
        fi

    fi

fi
