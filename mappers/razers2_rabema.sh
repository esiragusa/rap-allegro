unset CMD

# split filename without extension into substrings separated by _
IFS="_"
params=(${mapper%.*})
unset IFS

# binary
bin=${params[0]}

# recognition rate (default: 99)
sensitivity=${params[1]:=99}

# pigeonhole/swift (default: pigeonhole)
filter=${params[2]}
if [ "$filter" == "sw" ]
then
    unset filter
else
    filter="-t 0"
fi

unset result_suffix
case "${output_format}" in
#    sam-cigar)
#        bin=${bin}.cigar
#        ;;
    native)
        of=0
        result_suffix=".native"
        ;;
	*)
   		of=4
   		;;
esac

CMD="${MAPPERS_BIN_DIR}/${bin} ${filter} -vv -tc ${threads} -rr ${sensitivity} -i ${percid} -id -of ${of} ${REFERENCE} -o ${RESULT}${result_suffix}"
case ${mode:=default} in
    all)
        CMD+=" -m 1000000"
        ;;
    best)
        CMD+=" -m 100 -dr 1"
        ;;
    any)
        CMD+=" -m 1 -dr 1"
        ;;
esac

# Check whether we want single-end or paired-end mode.
if [ -z "${READS2}" ]
then
    # Single-End
    CMD+=" ${READS}"
else
    # Paired-End
    CMD+=" --library-length ${INSERT_MEAN} --library-error ${INSERT_ERR}"
    CMD+=" ${READS} ${READS2}"
fi

# no preparation required
if [ "${mode}" == "prepare" ]
then
    unset CMD
fi

