unset CMD

unset library_options
unset output_options
if [ -z "${READS2}" ]; then
    # Single-End
    library_options="-i ${READS%.gz}"
    output_options="--expect-single-end-reads"
    compress_cmd="gzip -cd ${READS} > ${READS%.gz}"
    clean_cmd="dont_measure rm ${READS%.gz}"
else
    # Paired-End
    library_options="--paired-end-alignment --map-both-ends --min-insert-size ${INSERT_MIN} --max-insert-size ${INSERT_MAX} -1 ${READS%.gz} -2 ${READS2%.gz}"
    output_options="--expect-paired-end-reads"
    compress_cmd="gzip -cd ${READS} > ${READS%.gz}
                  gzip -cd ${READS2} > ${READS2%.gz}"
    clean_cmd="dont_measure rm ${READS%.gz}
               dont_measure rm ${READS2%.gz}"
fi


if [ "${mode}" == "prepare" ]
then

    if [ ${REFERENCE} -nt ${INDEX}.gem.gem ]; then
        CMD="${MAPPERS_BIN_DIR}/gem-indexer --threads ${threads} -i ${REFERENCE} -o ${INDEX}.gem"
    fi

else

    unset mapping_options
    case ${mode:=default} in
        all)
            mapping_options="-d all -D all -s all --max-big-indel-length 0"
            ;;
        best)
            mapping_options="-s 0"
            ;;
        *)
            mapping_options=""
            ;;
    esac

    CMD="${compress_cmd}
         ${MAPPERS_BIN_DIR}/gem-mapper --threads ${threads} --quality-format ignore -m ${erate} -e ${erate} -I ${INDEX}.gem.gem ${mapping_options} ${library_options} | \
         ${MAPPERS_BIN_DIR}/gem-2-sam --threads ${threads} --quality-format offset-33 --sequence-lengths --index ${INDEX}.gem.gem ${output_options} | \
         samtools view -@ ${threads} -bS - > ${RESULT}
         ${clean_cmd}"

fi
