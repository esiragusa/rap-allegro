unset CMD

if [ -z "${READS2}" ]; then
    # Single-End
    unset library_options
    library_fragment="-U <( zcat ${READS} )"
else
    # Paired-End
    library_options="--minins ${INSERT_MIN} --maxins ${INSERT_MAX}"
    library_fragment="-1 <( zcat ${READS} ) -2 <( zcat ${READS2} )"
fi


if [ "${mode}" == "prepare" ]
then

    if [ ${REFERENCE} -nt ${INDEX}.bowtie2.1.bt2 ]; then
        CMD="${MAPPERS_BIN_DIR}/bowtie2-build ${REFERENCE} ${INDEX}.bowtie2"
    fi

else

    unset options
    case ${mode:=default} in
        all)
            options="--end-to-end -k 100"
            ;;
        best)
            options="--end-to-end"
            ;;
        *)
            options=""
            ;;
    esac

    CMD="${MAPPERS_BIN_DIR}/bowtie2-align-s -p ${threads} ${options} ${library_options} ${INDEX}.bowtie2 ${library_fragment} --rg-id none --rg SM:none | samtools view -@ ${threads} -bS - > ${RESULT}"

fi
