# split filename without extension into substrings separated by _
IFS="_"
params=(${mapper%.*})
unset IFS

bin=${params[0]}
strata=${params[1]}
index_prefix=${INDEX}.yara #${bin}

unset CMD
if [ "${mode}" == "prepare" ]
then

    if [[ ! -e ${index_prefix}.${index_file} || ${REFERENCE} -nt ${index_prefix}.${index_file} ]]
    then
        CMD="${MAPPERS_BIN_DIR}/yara_indexer --output-prefix ${index_prefix} ${REFERENCE}
             dont_measure chmod g+rw ${index_prefix}.*"
    fi

else

    unset strategy
    case ${mode:=default} in
        all)
            strategy="--output-secondary --output-rabema --all"
            ;;
        best)
            strategy="--output-secondary --output-rabema"
            ;;
        *)
            strategy=""
            ;;
    esac

    output_opts=""
    if [ "${strata}" == "strata" ]
    then
        output_opts="--strata-rate 1"
    fi

    f=$(echo "${erate}*100"|bc)
    error_rate=${f%.*}

    # Check whether we want single-end or paired-end mode.
    unset reads
    unset library
    if [ -z "${READS2}" ]
    then
        # Single-End
        reads=" ${READS}"
    else
        # Paired-End
        library=" --library-length ${INSERT_MEAN} --library-error ${INSERT_ERR}"
        reads=" ${READS} ${READS2}"
    fi

    CMD="${MAPPERS_BIN_DIR}/${bin}_mapper ${strategy} ${output_opts} --verbose --threads ${threads} --error-rate ${error_rate} ${library} --output-file ${RESULT} ${index_prefix} ${reads}
         dont_measure chmod g+rw ${RESULT}"

fi
